import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Perfil } from '../models/whatsapp.model';

const host = 'http://localhost';
const port = '8080';

@Injectable({
  providedIn: 'root'
})
export class WhatsappService {

  apiUrl = `${host}:${port}`;

  constructor(
    private HttpClient: HttpClient
  ) { }

  public getPerfil(): Observable<Perfil> {
    return this.HttpClient.get<Perfil>(`${this.apiUrl}/get-perfil`);
  }
  
  public setLogin(): Observable<Perfil> {
    return this.HttpClient.get<Perfil>(`${this.apiUrl}/set-login`);
  }
  
  public setLogout(): Observable<Perfil> {
    return this.HttpClient.get<Perfil>(`${this.apiUrl}/set-logout`);
  }

}
