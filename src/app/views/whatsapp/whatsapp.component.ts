import { Component, OnInit } from '@angular/core';
import { Perfil } from 'src/app/models/whatsapp.model';
import { WhatsappService } from 'src/app/services/whatsapp.service'
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-whatsapp',
  templateUrl: './whatsapp.component.html',
  styleUrls: ['./whatsapp.component.scss']
})
export class WhatsappComponent implements OnInit {
  perfil: Perfil = new Perfil;

  contatos = [
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Gabriel', telefone: '61994626754'},
    {nome: 'Pedro', telefone: '61994626754'},
    {nome: 'Julia', telefone: '61994626754'}
  ];

  constructor(
    private WhatsappService: WhatsappService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    // this.getPerfil();
  }

  private getPerfil() {
    this.spinner.show();
    this.WhatsappService.getPerfil().subscribe((data) => {
      this.perfil = data;
      this.spinner.hide();
    })
  }

  setLogin() {
    this.spinner.show();
    this.WhatsappService.setLogin().subscribe((data) => {
      this.perfil = data;
      this.spinner.hide();
    })
  }
  
  setLogout() {
    this.spinner.show();
    this.WhatsappService.setLogout().subscribe((data) => {
      this.perfil = new Perfil;
      this.spinner.hide();
    })
  }

}
